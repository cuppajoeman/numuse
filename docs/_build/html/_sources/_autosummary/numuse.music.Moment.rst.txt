numuse.music.Moment
===================

.. currentmodule:: numuse.music

.. autoclass:: Moment
   :members:                                    
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Moment.__init__
   
   

   
   
   