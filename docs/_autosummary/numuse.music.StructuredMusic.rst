numuse.music.StructuredMusic
============================

.. currentmodule:: numuse.music

.. autoclass:: StructuredMusic
   :members:                                    
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~StructuredMusic.__init__
      ~StructuredMusic.bpm_to_measure_length
   
   

   
   
   