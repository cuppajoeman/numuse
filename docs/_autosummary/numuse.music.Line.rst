numuse.music.Line
=================

.. currentmodule:: numuse.music

.. autoclass:: Line
   :members:                                    
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Line.__init__
   
   

   
   
   