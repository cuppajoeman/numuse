numuse.music.Music
==================

.. currentmodule:: numuse.music

.. autoclass:: Music
   :members:                                    
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Music.__init__
      ~Music.bpm_to_measure_length
   
   

   
   
   