numuse.music
============

.. automodule:: numuse.music
  
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
      :toctree:                                          
      :template: custom-class-template.rst               
   
      Line
      Measure
      Moment
      Music
      StructuredMusic
   
   

   
   
   




