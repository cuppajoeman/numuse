numuse.music.Measure
====================

.. currentmodule:: numuse.music

.. autoclass:: Measure
   :members:                                    
   :show-inheritance:                           
   :inherited-members:                          

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~Measure.__init__
   
   

   
   
   